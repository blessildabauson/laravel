<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/user', 'UserController@index')->name('user');
Route::get('/user/{id}/edit/', 'UserController@edit')->name('user.edit');
Route::get('/user/{id}/delete/', 'UserController@delete')->name('user.delete');
Route::get('/user/add/', 'UserController@add')->name('user.add');
Route::get('/user/export/{type}', 'UserController@exportUsers')->name('user.exportUsers');
