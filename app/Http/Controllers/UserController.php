<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;
use View;
use DB;
use Excel;

class UserController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //upload image
        $input   = $request->all();
        $imgname = "";
        if($file = $request->file('image')){
            $imgname         = time().'_'.$file->getClientOriginalName();
            $destinationPath = public_path('/images');
            $file->move($destinationPath, $imgname);

        }

        //update entry
        if(isset($_POST['id']) && !empty($_POST['id'])){
            try{
                //Find the user object from model if it exists
                $userObj  = new User();
                $user     = $userObj->findOrFail($_POST['id']);

                $user->first_name   = $_POST['first_name'];
                $user->last_name    = $_POST['last_name'];
                $user->email        = $_POST['email'];
                $user->image_file   = $imgname;

                if(!empty($_POST['password'])){
                    $user->password  = Hash::make($_POST['first_name']);
                }
                $user->save();
                return redirect()->route('home');
            }
            catch(ModelNotFoundException $err){
                //Show error page
            }

        } else {
            //insert new entry in the table
            $user = new User();
            $user->fill($request->all());
            $user->create([
                'first_name'    => $_POST['first_name'],
                'last_name'     => $_POST['last_name'],
                'email'         => $_POST['email'],
                'image_file'    => $imgname,
                'password'      => Hash::make($_POST['password']),
            ]);
            return redirect()->route('home');
        }
    }

    public function edit($id)
    {
        if(!empty($id)){
            $user  = new User();
            $data  = $user->where('id', $id)->first();
            return view::make('user', compact('data'));
        }
    }

    public function add()
    {
        return view::make('add', compact('data'));
    }

    public function delete($id)
    {
        $user  = new User();
        $user->where('id', $id)->delete();
        return redirect()->route('home');
    }

    public function exportUsers($type)
    {

        $user  = new User();
        $data  = $user->get()->toArray();
        
        return Excel::create('User Export', function($excel) use ($data) {
            $excel->sheet('userSheet', function($sheet) use ($data){
                $sheet->fromArray($data);
            });
        })->download($type);
    }
   
}