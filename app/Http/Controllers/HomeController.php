<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user  = new User();
        //$users = $user->all();
        $users = $user->sortable()->paginate(2);
        return view::make('home', compact('users'));
    }
}
