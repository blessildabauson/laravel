@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="margin-bottom:15px;">
            <div class="row">
                <div class="col-md-6" style="text-align:left;">
                    <a href="{{ url('user/add') }}"><i class="fas fa-user-plus"></i> {{ __('Add New User') }}</a>
                </div>
                <div class="col-md-6" style="text-align:right;">
                    <div class="btn-group">
                        <button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Export All Data <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="{{ URL::to('/user/export/xls') }}">Download XLS</a></li>
                          <li><a href="{{ URL::to('/user/export/xlsx') }}">Download XLSX</button></li>
                          <li><a href="{{ URL::to('/user/export/csv') }}">Download CSV</a></li>
                        </ul>
                    </div>
                </div>
            </a>
        </div><br clear="all"/>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Users') }}</div>
                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>@sortablelink('first_name',  __('First Name'))</th>
                                <th>@sortablelink('last_name', __('Last Name'))</th>
                                <th>@sortablelink('email', __('Email'))</th>
                                <th>{{ __('Action') }} </th>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($users as $user)
                              <tr>
                                    <td style="text-align:center;"> 
                                        @if(!empty($user->image_file))
                                          <img src="/images/{{$user->image_file}}" style="width:60px; height:60px;">
                                        @else
                                          <img src="/images/nophoto.png" style="width:60px; height:60px;">
                                        @endif
                                    </td>
                                    <td> {{$user->first_name}} </td>
                                    <td> {{$user->last_name}} </td>
                                    <td> {{$user->email}} </td>
                                    <td style="text-align:center;">
                                        <a href="{{ url('user/'.$user->id.'/edit') }}"> <i class="far fa-edit" title="{{ __('Edit') }}"></i> </a> 
                                        <a href="{{ url('user/'.$user->id.'/delete') }}"> <i class="far fa-trash-alt" title="{{ __('Delete') }}"></i> </a>
                                    </td>
                              </tr>
                             @endforeach
                       </tbody>
                    </table>
                    {!! $users->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
